@echo off

if exist %CommonProgramFiles(x86)% (
	set WINDOWS="WIN64"
) else (
	set WINDOWS="WIN32"
)

if "%WINDOWS%" == "WIN64" (
	set VS2017COMMUINTY="C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2017PROFESSIONAL="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2017ENTERPRISE="C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\VC\Auxiliary\Build\vcvarsall.bat"
) else (
	set VS2017COMMUINTY="C:\Program Files\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2017PROFESSIONAL="C:\Program Files\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2017ENTERPRISE="C:\Program Files\Microsoft Visual Studio\2017\Enterprise\VC\Auxiliary\Build\vcvarsall.bat"
)

if "%WINDOWS%" == "WIN64" (
	set VS2019COMMUINTY="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2019PROFESSIONAL="C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2019ENTERPRISE="C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat"
) else (
	set VS2019COMMUINTY="C:\Program Files\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2019PROFESSIONAL="C:\Program Files\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvarsall.bat"
	set VS2019ENTERPRISE="C:\Program Files\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat"
)

set VS2022COMMUNITY="C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat"
set VS2022PROFESSIONAL="C:\Program Files\Microsoft Visual Studio\2022\Professional\VC\Auxiliary\Build\vcvarsall.bat"
set VS2022ENTERPRISE="C:\Program Files\Microsoft Visual Studio\2022\Enterprise\VC\Auxiliary\Build\vcvarsall.bat"


if not defined DevEnvDir (
	if exist %VS2022ENTERPRISE% (
		call %VS2022ENTERPRISE% x64
	) else (
		if exist %VS2022PROFESSIONAL% (
			call %VS2022PROFESSIONAL% x64
		) else (
			if exist %VS2022COMMUNITY% (
				call %VS2022COMMUNITY% x64
			) else (
				if exist %VS2017COMMUINTY% (
					call %VS2017COMMUINTY% x64
				) else (
					if exist %VS2017PROFESSIONAL% (
						call %VS2017PROFESSIONAL% x64
					) else (
						if exist %VS2019COMMUINTY% (
							call %VS2019COMMUINTY% x64
						) else (
							if exist %VS2019PROFESSIONAL% (
								call %VS2019PROFESSIONAL% x64
							) else (
								if exist %VS2019ENTERPRISE% (
									call %VS2019ENTERPRISE% x64
								)
							)
						)
					)
				)
			)
		)
	)
)

set ROOT=%cd%
REM /Bt+  /d1reportTime

set CFLAGS= -MTd -Z7 -GS- -Oi -Gm- -GR- -sdl- -FC -I%ROOT% -showIncludes -MP -Wall -analyze /d2cgsummary -Bt+
set DEFINES=
set LFLAGS=-link -heap:134217728,67108864 -stack:33554432,16777216 -SUBSYSTEM:CONSOLE -NXCOMPAT:NO -DYNAMICBASE:NO -opt:ref -opt:icf -out:output.exe
set WIN32_LIBS= opengl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib crypt32.lib setupapi.lib imm32.lib winmm.lib version.lib d2d1.lib ole32.lib Netapi32.lib userenv.lib dwmapi.lib wtsapi32.lib
set LIBS=
set SOURCES=%ROOT%/*.c

if exist debug\x86 (
rmdir /S /Q debug\x86
mkdir debug\x86
pushd debug\x86
cl %DEFINES% %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug\x86
) else (
mkdir debug\x86
pushd debug\x86
cl %DEFINES% %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug\x86
)




