#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>

#if defined(_WIN32) || defined(_WIN64) && defined(_MSC_VER)

#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <windows.h>

#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"iphlpapi.lib")

#define CLOSE_SOCKET(x) closesocket(x)

typedef SOCKET socket_t;

void winsock_init ( )
{
	WSADATA wsa = { 0 };
	int result = WSAStartup ( MAKEWORD ( 2, 2 ), &wsa );
    if ( !result ) 
	{
        fprintf ( stderr, "WSAStartup failed: %d\n", result );
        return;
    }
}

void winsock_deinit ( )
{
	WSACleanup ( );
	return;
}

#else
typedef int socket_t;
#define CLOSE_SOCKET(x) close(x)


#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif /* !defined(_WIN32) || defined(_WIN64) && defined(_MSC_VER) */


#define SSDP_MULTICAST_ADDRESS      "239.255.255.250"
#define SSDP_MULTICAST_PORT         1900
#define MAX_BUFFER_LENGTH           8192

static const char ssdp_discover_string [ ] =
    "M-SEARCH * HTTP/1.1\r\n"
    "HOST: 239.255.255.250:1900\r\n"
    "MAN: \"ssdp:discover\"\r\n"
    "MX: 3\r\n"
    "ST: ssdp:all\r\n"
    "\r\n";

typedef struct vector_t
{
    char **array;
    int  count;
	
} vector_t;

int vector_init ( vector_t *v )
{
    v->array = 0;
    v->count = 0;
    return   ( 0 );
}

int vector_add ( vector_t *v, char *str )
{
    if ( !( v->array = ( char ** ) realloc ( v->array, ( v->count + 1 ) * sizeof ( char * ) ) ) )
    {
        return ( -1 );
    }

    if ( !( v->array [ v->count++ ] = strdup ( str ) ) )
    {
        return ( -1 );
    }

    return ( 0 );
}

int vector_search ( vector_t *v, char *search_str )
{
    int i;

    for ( i = 0; i < v->count; i++ )
    {
        if ( strcmp ( v->array [ i ], search_str ) == 0 )
		{
			return ( true );
		}
    }
    return ( false );
}

int vector_free ( vector_t *v )
{
    int i;
    for ( i = 0; i < v->count; i++ )
    {
        free ( v->array [ i ] );
    }
    free ( v->array );
    v->count = 0;
    return ( 0 );
}

int send_ssdp_request ( socket_t sock )
{
    int ret       = 0;
	int len       = 0;
	int bytes_out = 0;
    struct sockaddr_in dsai = { 0 };

    memset       ( &dsai, 0, sizeof ( dsai ) );
    dsai.sin_family = AF_INET;
    dsai.sin_port   = htons ( SSDP_MULTICAST_PORT );
    inet_pton    ( AF_INET, SSDP_MULTICAST_ADDRESS, &dsai.sin_addr );
    len = strlen ( ssdp_discover_string );

    if ( ( bytes_out = sendto ( sock, ssdp_discover_string, len, 0, ( struct sockaddr* ) &dsai, sizeof ( dsai ) ) ) < 0 ) 
    {
        perror ( "sendto()" );
        return ( errno );
    }
    else if ( bytes_out != len ) 
    {
        fprintf ( stderr, "sendto(): only sent %d of %d bytes\n", bytes_out, len );
        return ( -1 );
    }
	printf("%s\n", ssdp_discover_string);
    return 0;
}


int get_ssdp_responses ( socket_t sock, bool dns_lookup, vector_t *v )
{
    int ret      = 0;
	int bytes_in = 0; 
	bool done    = false;
	
    uint32_t host_sock_len;
    struct sockaddr_in host_sock      = { 0 };
    char buffer [ MAX_BUFFER_LENGTH	] = { 0 };
    char host   [ NI_MAXHOST ]        = { 0 };
    
	char *url_start  = 0;
	char *host_start = 0; 
	char *host_end   = 0;
	
    fd_set read_fds        = { 0 };
    struct timeval timeout = { 0 };
	

    FD_ZERO ( &read_fds );
    FD_SET  ( sock, &read_fds );
    timeout.tv_sec  = 5;
    timeout.tv_usec = 0;

    /* Loop through SSDP discovery request responses */
    do
    {
        if ( ( ret = select ( sock + 1, &read_fds, 0, 0, &timeout ) ) < 0 ) 
        {
            perror ( "select()" );
            return ( errno );
        }

        if ( FD_ISSET ( sock, &read_fds ) )
        {
            host_sock_len = sizeof ( host_sock );
            if ( ( bytes_in = recvfrom ( sock, buffer, sizeof ( buffer ), 0, ( struct sockaddr * ) &host_sock, &host_sock_len ) ) < 0 )
            {
                perror ( "recvfrom()" );
                return ( errno );
            }
            buffer [ bytes_in ] = '\0';

            /* Parse valid responses */
            if ( !strncmp ( buffer, "HTTP/1.1 200 OK", 12 ) )
            {
				printf ( "\n%s", buffer );
				
                /* Skip ahead to url in SSDP response */
                if ( ( url_start = strstr ( buffer, "LOCATION:" ) ) != 0 )
                {
                    /* Get hostname/IP address in SSDP response */
                    if ( ( host_start = strstr ( url_start, "http://" ) ) != 0 )
                    {
                        host_start += 7;    /* Move past "http://" */

                        if ( ( host_end = strstr ( host_start, ":" ) ) != 0 )
                        {
                            strncpy ( host, host_start, host_end - host_start );
                            host    [ host_end - host_start ] = '\0';

                            /* Add host to vector if we haven't done so already */
                            if ( vector_search ( v, host ) == false )
                            {
                                vector_add ( v, host    );
                                printf     ( "%s", host );

                                /* Are we doing reverse lookups? */
                                if ( dns_lookup == true )
                                {
                                    char name [ NI_MAXHOST ] = { 0 };
                                    if ( ( rdns_lookup ( host, name, NI_MAXHOST ) ) == 0 )
                                    {
                                        printf ( "\t%s", name );
                                    }
                                }

                                printf("\n");
                            }
                        }
                    }
                }
            }
            else
            {
                fprintf ( stderr, "[Unexpected SSDP response]\n" );
				printf  ( "%s\n\n", buffer );
            }
        } 
        else 
        {
            done = true;
        }

    } while ( done == false );

    return ret;
}



int rdns_lookup ( char *ip_addr, char *hostname, int hostname_size )
{
    int ret               =   0;
    struct sockaddr_in sa = { 0 };
    memset ( &sa, 0, sizeof ( sa ) );
    sa.sin_family = AF_INET;
    inet_pton ( AF_INET, ip_addr, &sa.sin_addr );
    if ( ( ret = getnameinfo ( ( struct sockaddr* ) &sa, sizeof ( sa ), hostname, hostname_size, 0, 0, 0 ) ) != 0 )
    {
        fprintf ( stderr, "getnameinfo(): %s\n", gai_strerror ( ret ) );
    }
    return ( ret );
}

int discover_hosts ( int port, bool dns_lookup, vector_t *v )
{
	socket_t sock = 0;
	struct sockaddr_in sai = { 0 };
	
#if defined(_WIN32)
	winsock_init ( );
#endif
	
	
	if ( ( sock = socket ( PF_INET, SOCK_DGRAM, 0 ) ) < 0 ) 
    {
        perror ( "socket()" );
        return ( errno );
    }
	
	memset ( &sai, 0, sizeof ( sai ) );
    sai.sin_family      = AF_INET;
    sai.sin_addr.s_addr = htonl ( INADDR_ANY );
    sai.sin_port        = htons ( port );
	
	if ( ( bind ( sock, ( struct sockaddr* ) &sai, sizeof ( sai ) ) ) != 0 ) 
    {
        perror ( "bind()" );
		CLOSE_SOCKET ( sock );
        return errno;
    }
	
	printf ( "[ Client bound to port %d ]\n\n", port );
	
    if ( ( send_ssdp_request ( sock ) ) != 0 )
    {
		CLOSE_SOCKET ( sock );
		return errno;
    }

    if ( ( get_ssdp_responses ( sock, dns_lookup, v ) ) != 0 )
    {
		CLOSE_SOCKET ( sock );
		return errno;
    }
	
	CLOSE_SOCKET ( sock );
#if defined(_WIN32)
	winsock_deinit ( );
#endif
	
	return 0;
}


int main  ( int argc, char **argv, char **envp )
{
	if ( argc < 2 )
	{
		printf ( "%s port\n", argv [ 0 ] );
		exit ( EXIT_FAILURE );
	}
	
    vector_t v = { 0 };
    vector_init ( &v );
    discover_hosts ( atoi ( argv [ 1 ] ), true, &v );
    vector_free ( &v );

	return 0;
}
